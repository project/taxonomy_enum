# Taxonomy Enum


## Introduction

This module allows you to synchronise taxonomy vocabularies with PHP enums. This is especially useful if you have a taxonomy vocabulary with a fixed set of terms and custom code that needs to behave differently based on which term is referenced. It basically allows you to reference a taxonomy term in code without having to hardcode ID's, which makes your code more portable between environments.


## Requirements

This module requires the core Taxonomy and the contrib [Taxonomy Machine Name](https://www.drupal.org/project/taxonomy_machine_name) modules.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Features
- Automatically creates terms based on the enum cases.
- Automatically deletes respective terms once enum cases have been removed (configurable).
- Disables the possibility to manually create/delete terms (configurable).
- Stores default field values in config as machine names instead of entity UUID's.
- Allows you to get enum instances in custom code:
  ```
  $entity->get('some_taxonomy_enum_field')->enum
  ```
- Contains code to easily migrate existing entity reference fields:
  ```
  \Drupal::service('taxonomy_enum.migration')->migrateField('field_tags', 'node');
  ```


## Maintainers

Current maintainers:
- Dieter Holvoet - [dieterholvoet](https://www.drupal.org/u/dieterholvoet)
