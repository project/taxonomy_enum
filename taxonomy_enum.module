<?php

/**
 * @file
 * Hook implementations for the taxonomy_enum module.
 */

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Session\AccountInterface;
use Drupal\field\FieldStorageConfigInterface;
use Drupal\taxonomy\VocabularyInterface;
use Drupal\taxonomy_enum\Exception\InvalidEnumException;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function taxonomy_enum_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the taxonomy_enum module.
    case 'help.page.taxonomy_enum':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('This module allows you to synchronise taxonomy vocabularies with <a href="@url">PHP enums</a>. This is especially useful if you have a taxonomy vocabulary with a fixed set of terms and custom code that needs to behave differently based on which term is referenced. It basically allows you to reference a taxonomy term in code without having to hardcode IDs, which makes your code more portable between environments.', ['@url' => 'https://stitcher.io/blog/php-enums']) . '</p>';
      $output .= '<h3>' . t('Features') . '</h3>';
      $output .= '<ul><li>' . t('Automatically creates terms based on the enum cases') . '</li>';
      $output .= '<li>' . t('Automatically deletes respective terms once enum cases have been removed (configurable)') . '</li>';
      $output .= '<li>' . t('Disables the possibility to manually create/delete terms (configurable)') . '</li>';
      $output .= '<li>' . t('Stores default field values in config as machine names instead of entity UUIDs') . '</li>';
      $output .= '<li>' . t('Allows you to get enum instances in custom code: @snippet', ['@snippet' => Markup::create('<pre>$entity->get(\'some_taxonomy_enum_field\')->enum</pre>')]) . '</li>';
      $output .= '<li>' . t('Contains code to easily migrate existing entity reference fields') . '</li></ul>';
      $output .= '<h3>' . t('More Information') . '</h3>';
      $output .= '<p>' . t('For more information about this module please visit the <a href="@link">module page</a>.', ['@link' => 'https://www.drupal.org/project/taxonomy_enum']) . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements @see hook_module_implements_alter().
 */
function taxonomy_enum_module_implements_alter(array &$implementations, string $hook): void {
  if ($hook == 'form_taxonomy_term_form_alter' && isset($implementations['taxonomy_enum'])) {
    // Move taxonomy_enum_form_taxonomy_term_form_alter to the end of the list.
    $group = $implementations['taxonomy_enum'];
    unset($implementations['taxonomy_enum']);
    $implementations['taxonomy_enum'] = $group;
  }
}

/**
 * Implements @see hook_form_BASE_FORM_ID_alter().
 */
function taxonomy_enum_form_taxonomy_vocabulary_form_alter(array &$form, FormStateInterface $formState): void {
  /** @var \Drupal\taxonomy\VocabularyInterface $type */
  $type = $formState->getFormObject()->getEntity();

  $form['taxonomy_enum'] = [
    '#type' => 'details',
    '#title' => t('Enum'),
    '#group' => 'additional_settings',
    '#open' => TRUE,
  ];

  $form['taxonomy_enum']['enum_name'] = [
    '#type' => 'textfield',
    '#title' => t('Enum class'),
    '#default_value' => $type->getThirdPartySetting('taxonomy_enum', 'enum_name'),
    '#description' => t('Enum class to sync this vocabulary with.'),
  ];

  $form['taxonomy_enum']['lock_terms'] = [
    '#type' => 'checkbox',
    '#title' => t('Lock terms'),
    '#default_value' => $type->getThirdPartySetting('taxonomy_enum', 'lock_terms'),
    '#description' => t('Prevent users from manually creating/editing/deleting terms of this type.'),
  ];

  $form['taxonomy_enum']['delete_orphaned_terms'] = [
    '#type' => 'checkbox',
    '#title' => t('Delete orphaned terms'),
    '#default_value' => $type->getThirdPartySetting('taxonomy_enum', 'delete_orphaned_terms', FALSE),
    '#description' => t('Automatically delete terms with no corresponding enum case.'),
  ];

  $form['#entity_builders'][] = 'taxonomy_enum_form_taxonomy_vocabulary_form_alter_entity_builder';
  $form['#validate'][] = 'taxonomy_enum_form_taxonomy_vocabulary_form_alter_validate';
}

/**
 * Entity builder for saving the third party settings provided by this module.
 *
 * @param string $entity_type_id
 *   The entity type identifier.
 * @param \Drupal\taxonomy\VocabularyInterface $entity
 *   The vocabulary updated with the submitted values.
 * @param array $form
 *   The complete form array.
 * @param \Drupal\Core\Form\FormStateInterface $formState
 *   The current state of the form.
 *
 * @see taxonomy_enum_form_taxonomy_vocabulary_form_alter()
 */
function taxonomy_enum_form_taxonomy_vocabulary_form_alter_entity_builder(string $entity_type_id, VocabularyInterface $entity, array &$form, FormStateInterface $formState): void {
  $class = $formState->getValue('enum_name');

  if ($class === '') {
    $entity->unsetThirdPartySetting('taxonomy_enum', 'enum_name');
    $entity->unsetThirdPartySetting('taxonomy_enum', 'lock_terms');
    $entity->unsetThirdPartySetting('taxonomy_enum', 'delete_orphaned_terms');

    return;
  }

  $entity->setThirdPartySetting('taxonomy_enum', 'enum_name', $class);
  $entity->setThirdPartySetting('taxonomy_enum', 'lock_terms', $formState->getValue('lock_terms'));
  $entity->setThirdPartySetting('taxonomy_enum', 'delete_orphaned_terms', $formState->getValue('delete_orphaned_terms'));
}

/**
 * Form validation handler.
 *
 * @param array $form
 *   An associative array containing the structure of the form.
 * @param \Drupal\Core\Form\FormStateInterface $formState
 *   The current state of the form.
 *
 * @see taxonomy_enum_form_taxonomy_vocabulary_form_alter()
 */
function taxonomy_enum_form_taxonomy_vocabulary_form_alter_validate(array &$form, FormStateInterface $formState): void {
  $enumClass = $formState->getValue('enum_name');
  if ($enumClass === NULL || $enumClass === '') {
    return;
  }

  try {
    $manager = \Drupal::getContainer()->get('taxonomy_enum.manager');
    $manager->createTerms($formState->getValue('vid'), $enumClass);

    if ($formState->getValue('delete_orphaned_terms')) {
      $manager->deleteOrphanedTerms($enumClass);
    }
  }
  catch (InvalidEnumException $exception) {
    $formState->setErrorByName('enum_name', $exception->getMessage());
  }
}

/**
 * Implements @see hook_ENTITY_TYPE_insert().
 */
function taxonomy_enum_taxonomy_vocabulary_insert(VocabularyInterface $entity): void {
  $enumClass = $entity->getThirdPartySetting('taxonomy_enum', 'enum_name');
  if ($enumClass === NULL) {
    return;
  }

  try {
    $manager = \Drupal::getContainer()->get('taxonomy_enum.manager');
    $manager->createTerms($entity->id(), $enumClass);

    if ($entity->getThirdPartySetting('taxonomy_enum', 'delete_orphaned_terms')) {
      $manager->deleteOrphanedTerms($enumClass);
    }
  }
  catch (InvalidEnumException) {
    return;
  }
}

/**
 * Implements @see hook_ENTITY_TYPE_update().
 */
function taxonomy_enum_taxonomy_vocabulary_update(VocabularyInterface $entity): void {
  $enumClass = $entity->getThirdPartySetting('taxonomy_enum', 'enum_name');
  if ($enumClass === NULL) {
    return;
  }

  try {
    $manager = \Drupal::getContainer()->get('taxonomy_enum.manager');
    $manager->createTerms($entity->id(), $enumClass);

    if ($entity->getThirdPartySetting('taxonomy_enum', 'delete_orphaned_terms')) {
      $manager->deleteOrphanedTerms($enumClass);
    }
  }
  catch (InvalidEnumException) {
    return;
  }

}

/**
 * Implements @see hook_entity_create_access().
 */
function taxonomy_enum_entity_create_access(AccountInterface $account, array $context, ?string $bundle): AccessResultInterface {
  if ($context['entity_type_id'] !== 'taxonomy_term') {
    return AccessResult::neutral();
  }

  if ($bundle === NULL) {
    return AccessResult::neutral();
  }

  $bundleEntity = \Drupal::entityTypeManager()
    ->getStorage('taxonomy_vocabulary')
    ->load($bundle);

  if (!$bundleEntity->getThirdPartySetting('taxonomy_enum', 'lock_terms')) {
    return AccessResult::neutral();
  }

  return AccessResult::forbidden('Term creation is locked because it is backed by an enum.');
}

/**
 * Implements hook_field_info_alter().
 *
 * @todo Remove once minimum version supported is at least 10.2.0.
 */
function taxonomy_enum_field_info_alter(array &$info): void {
  // Allow module to work with versions of older versions of Drupal.
  if (\version_compare(\Drupal::VERSION, '10.1.9999', '<')) {
    $info['taxonomy_enum']['category'] = t('Reference');
  }
}

/**
 * Implements @see hook_field_widget_info_alter().
 */
function taxonomy_enum_field_widget_info_alter(array &$info): void {
  foreach ($info as &$fieldInfo) {
    if (in_array('entity_reference', $fieldInfo['field_types'], TRUE)) {
      $fieldInfo['field_types'][] = 'taxonomy_enum';
    }
  }
}

/**
 * Implements @see hook_field_formatter_info_alter().
 */
function taxonomy_enum_field_formatter_info_alter(array &$info): void {
  foreach ($info as &$fieldInfo) {
    if (in_array('entity_reference', $fieldInfo['field_types'], TRUE)) {
      $fieldInfo['field_types'][] = 'taxonomy_enum';
    }
  }
}

/**
 * Implements @see hook_field_diff_builder_info_alter().
 */
function taxonomy_enum_field_diff_builder_info_alter(array &$diffBuilders): void {
  $diffBuilders['entity_reference_field_diff_builder']['field_types'][] = 'taxonomy_enum';
}

/**
 * Implements @see hook_form_BASE_FORM_ID_alter().
 */
function taxonomy_enum_form_taxonomy_term_form_alter(array &$form, FormStateInterface $formState): void {
  /** @var \Drupal\taxonomy\TermInterface $entity */
  $entity = $formState->getBuildInfo()['callback_object']->getEntity();
  /** @var \Drupal\taxonomy\VocabularyInterface $vocabulary */
  $vocabulary = $entity->get('vid')->entity;

  if (!$vocabulary->getThirdPartySetting('taxonomy_enum', 'lock_terms')) {
    return;
  }

  if (!isset($form['machine_name'])) {
    return;
  }

  $form['machine_name']['#disabled'] = TRUE;
}

/**
 * Implements hook_field_views_data_alter().
 *
 * @see core_field_views_data()
 */
function taxonomy_enum_field_views_data_alter(array &$data, FieldStorageConfigInterface $field_storage): void {
  // The code below only deals with the Taxonomy enum field type.
  if ($field_storage->getType() != 'taxonomy_enum') {
    return;
  }

  $entity_type_manager = \Drupal::entityTypeManager();
  $entity_type_id = $field_storage->getTargetEntityTypeId();
  /** @var \Drupal\Core\Entity\Sql\DefaultTableMapping $table_mapping */
  $table_mapping = $entity_type_manager->getStorage($entity_type_id)->getTableMapping();

  foreach ($data as $table_name => $table_data) {
    // Add a relationship to the target entity type.
    $target_entity_type_id = $field_storage->getSetting('target_type');
    $target_entity_type = $entity_type_manager->getDefinition($target_entity_type_id);
    $entity_type_id = $field_storage->getTargetEntityTypeId();
    $entity_type = $entity_type_manager->getDefinition($entity_type_id);
    $target_base_table = $target_entity_type->getDataTable() ?: $target_entity_type->getBaseTable();
    $field_name = $field_storage->getName();

    if ($target_entity_type instanceof ContentEntityTypeInterface) {
      foreach ($table_data as $table_field_name => $table_field_data) {
        if (isset($table_field_data['filter']) && $table_field_name != 'delta') {
          // Create separate views data to allow use of the entity_reference
          // filter. Numeric filter should still be available for use.
          $entity_reference = $data[$table_name][$table_field_name];
          $entity_reference['filter']['id'] = 'taxonomy_enum_reference';
          $entity_reference['title'] = $entity_reference['title'] . ' ' . t('as an Enum filter');
          $data[$table_name][$table_field_name . '_enum'] = $entity_reference;
        }
      }

      // Provide a relationship for the entity type with the entity reference
      // field.
      $args = [
        '@label' => $target_entity_type->getLabel(),
        '@field_name' => $field_name,
      ];
      $data[$table_name][$field_name]['relationship'] = [
        'title' => t('@label referenced from @field_name', $args),
        'label' => t('@field_name: @label', $args),
        'group' => $entity_type->getLabel(),
        'help' => t('Appears in: @bundles.', ['@bundles' => implode(', ', $field_storage->getBundles())]),
        'id' => 'standard',
        'base' => $target_base_table,
        'entity type' => $target_entity_type_id,
        'base field' => $target_entity_type->getKey('id'),
        'relationship field' => $field_name . '_target_id',
      ];

      // Provide a reverse relationship for the entity type that is referenced by
      // the field.
      $args['@entity'] = $entity_type->getLabel();
      $args['@label'] = $target_entity_type->getSingularLabel();
      $pseudo_field_name = 'reverse__' . $entity_type_id . '__' . $field_name;
      $data[$target_base_table][$pseudo_field_name]['relationship'] = [
        'title' => t('@entity using @field_name', $args),
        'label' => t('@field_name', ['@field_name' => $field_name]),
        'group' => $target_entity_type->getLabel(),
        'help' => t('Relate each @entity with a @field_name set to the @label.', $args),
        'id' => 'entity_reverse',
        'base' => $entity_type->getDataTable() ?: $entity_type->getBaseTable(),
        'entity_type' => $entity_type_id,
        'base field' => $entity_type->getKey('id'),
        'field_name' => $field_name,
        'field table' => $table_mapping->getDedicatedDataTableName($field_storage),
        'field field' => $field_name . '_target_id',
        'join_extra' => [
          [
            'field' => 'deleted',
            'value' => 0,
            'numeric' => TRUE,
          ],
        ],
      ];
    }
  }
}
