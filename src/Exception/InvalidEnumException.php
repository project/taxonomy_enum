<?php

namespace Drupal\taxonomy_enum\Exception;

/**
 * Exception thrown when an invalid enum is used.
 */
class InvalidEnumException extends \InvalidArgumentException {
}
