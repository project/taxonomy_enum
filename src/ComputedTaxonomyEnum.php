<?php

namespace Drupal\taxonomy_enum;

use Drupal\Core\TypedData\TypedData;
use Drupal\taxonomy\TermInterface;
use Drupal\taxonomy\VocabularyInterface;

/**
 * Computed taxonomy enum property class.
 */
class ComputedTaxonomyEnum extends TypedData {

  /**
   * The data value.
   *
   * @var mixed
   */
  protected $value;

  /**
   * The enum.
   *
   * @var \BackedEnum|null
   */
  protected ?\BackedEnum $enum = NULL;

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    if ($this->enum !== NULL) {
      return $this->enum;
    }

    $term = $this->getParent()->entity;
    if (!$term instanceof TermInterface) {
      return NULL;
    }

    $vocabulary = $term->vid->entity;
    if (!$vocabulary instanceof VocabularyInterface) {
      return NULL;
    }

    $enumName = $vocabulary->getThirdPartySetting('taxonomy_enum', 'enum_name');
    $machineName = $term->get('machine_name')->value;

    if (enum_exists($enumName) && method_exists($enumName, 'tryFrom')) {
      return $this->enum = $enumName::tryFrom($machineName);
    }

    return NULL;
  }

}
