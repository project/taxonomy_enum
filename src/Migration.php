<?php

namespace Drupal\taxonomy_enum;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Helper methods to migrate entity_reference field types to taxonomy_enum.
 */
class Migration {

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager service.
   * @param \Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface $entityLastInstalledSchemaRepository
   *   The entity last installed schema repository service.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected EntityFieldManagerInterface $entityFieldManager,
    protected EntityLastInstalledSchemaRepositoryInterface $entityLastInstalledSchemaRepository,
  ) {
  }

  /**
   * Migrates an entity_reference field to taxonomy_enum field.
   *
   * @param string $entityTypeId
   *   The entity type ID.
   * @param string $fieldName
   *   The field name.
   */
  public function migrateEntityReferenceField(string $entityTypeId, string $fieldName): void {
    $fieldStorageConfig = $this->configFactory->getEditable("field.storage.$entityTypeId.$fieldName");

    if ($fieldStorageConfig->get('type') === 'taxonomy_enum') {
      return;
    }

    if ($fieldStorageConfig->get('type') !== 'entity_reference') {
      throw new \Exception('Not an entity reference field');
    }

    // Update the field storage.
    /** @var \Drupal\field\Entity\FieldStorageConfig[] $schemaDefinitions */
    $schemaDefinitions = $this->entityLastInstalledSchemaRepository->getLastInstalledFieldStorageDefinitions($entityTypeId);
    $schemaDefinitions[$fieldName]->set('type', 'taxonomy_enum');
    $this->entityLastInstalledSchemaRepository->setLastInstalledFieldStorageDefinitions($entityTypeId, $schemaDefinitions);

    $this->entityFieldManager->clearCachedFieldDefinitions();

    $fieldStorageConfig->set('type', 'taxonomy_enum');
    $fieldStorageConfig->set('module', 'taxonomy_enum');
    $fieldStorageConfig->save(TRUE);

    FieldStorageConfig::loadByName($entityTypeId, $fieldName)->calculateDependencies()->save();

    // Update the field instances.
    $fieldMap = $this->entityFieldManager->getFieldMapByFieldType('entity_reference')[$entityTypeId][$fieldName];

    foreach ($fieldMap['bundles'] as $bundle) {
      $fieldConfig = $this->configFactory->getEditable("field.field.$entityTypeId.$bundle.$fieldName");
      $fieldConfig->set('field_type', 'taxonomy_enum');
      $fieldConfig->save();

      /** @var \Drupal\field\FieldConfigInterface $fieldConfig */
      $fieldConfig = FieldConfig::loadByName($entityTypeId, $bundle, $fieldName);
      $fieldConfig->calculateDependencies()->save();
    }
  }

}
