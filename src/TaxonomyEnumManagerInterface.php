<?php

namespace Drupal\taxonomy_enum;

use Drupal\taxonomy\TermInterface;

/**
 * Interface for the taxonomy enum manager service.
 */
interface TaxonomyEnumManagerInterface {

  /**
   * Get the taxonomy term for the given enum.
   *
   * @param \BackedEnum $enum
   *   The enum.
   *
   * @return \Drupal\taxonomy\TermInterface|null
   *   The taxonomy term, or null if it does not exist.
   */
  public function getTermByEnum(\BackedEnum $enum): ?TermInterface;

  /**
   * Get the vocabulary ID for the given enum.
   *
   * @param class-string $enumName
   *   The enum name.
   */
  public function getVocabularyIdByEnumName(string $enumName): ?string;

  /**
   * Create terms for all given machine names, if they do not exist yet.
   *
   * @param string $vocabularyId
   *   The vocabulary machine name.
   * @param class-string $enumName
   *   The enum name.
   */
  public function createTerms(string $vocabularyId, string $enumName): void;

  /**
   * Delete terms that are missing from the given enum.
   *
   * @param class-string $enumName
   *   The enum name.
   */
  public function deleteOrphanedTerms(string $enumName): void;

}
