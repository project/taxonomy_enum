<?php

namespace Drupal\taxonomy_enum\Plugin\Field\FieldType;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines an item list class for taxonomy enum fields.
 */
class TaxonomyEnumItemList extends EntityReferenceFieldItemList {

  /**
   * {@inheritdoc}
   */
  public static function processDefaultValue($default_value, FieldableEntityInterface $entity, FieldDefinitionInterface $definition) {
    $defaultValue = parent::processDefaultValue($default_value, $entity, $definition);

    if ($defaultValue) {
      // Convert machine names to numeric IDs.
      $machineNames = [];
      foreach ($default_value as $delta => $properties) {
        if (isset($properties['enum'])) {
          $machineNames[$delta] = $properties['enum'];
        }
      }

      if ($machineNames) {
        $targetBundles = $definition->getSetting('handler_settings')['target_bundles'] ?? [];
        $terms = static::loadTermsByMachineNames($machineNames, $targetBundles);

        $entityMachineNames = [];
        foreach ($terms as $id => $term) {
          $entityMachineNames[$term->get('machine_name')->value] = $id;
        }

        foreach ($machineNames as $delta => $machineName) {
          if (isset($entityMachineNames[$machineName])) {
            $default_value[$delta]['target_id'] = $entityMachineNames[$machineName];
            unset($default_value[$delta]['target_uuid']);
          }
          else {
            unset($default_value[$delta]);
          }
        }
      }

      // Ensure we return consecutive deltas, in case we removed unknown UUIDs.
      $default_value = array_values($default_value);
    }

    return $default_value;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultValuesFormSubmit(array $element, array &$form, FormStateInterface $form_state) {
    $defaultValue = parent::defaultValuesFormSubmit($element, $form, $form_state);
    $entityRepository = \Drupal::getContainer()->get('entity.repository');

    // Convert UUIDs to machine names to ensure config deployability.
    foreach ($defaultValue as $delta => $properties) {
      $uuid = $properties['target_uuid'];
      $entity = $entityRepository->loadEntityByUuid('taxonomy_term', $uuid);

      $defaultValue[$delta]['enum'] = $entity->get('machine_name')->value;
      unset($defaultValue[$delta]['target_uuid']);
    }

    return $defaultValue;
  }

  /**
   * Loads terms by machine names.
   *
   * @param array $machineNames
   *   An array of machine names.
   * @param array $targetBundles
   *   An array of target bundles.
   *
   * @return array
   *   An array of terms.
   */
  protected static function loadTermsByMachineNames(array $machineNames, array $targetBundles): array {
    $storage = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term');
    $query = $storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('machine_name', $machineNames, 'IN');

    if ($targetBundles !== []) {
      $query->condition('vid', $targetBundles, 'IN');
    }

    $ids = $query->execute();
    if ($ids === []) {
      return [];
    }

    return $storage->loadMultiple($ids);
  }

}
