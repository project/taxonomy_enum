<?php

namespace Drupal\taxonomy_enum\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\taxonomy_enum\ComputedTaxonomyEnum;

/**
 * Plugin implementation of the 'taxonomy_enum' field type.
 *
 * @FieldType(
 *   id = "taxonomy_enum",
 *   label = @Translation("Taxonomy term backed by enum"),
 *   description = @Translation("An entity field containing an reference to a taxonomy term backed by an enum."),
 *   category = "reference",
 *   default_widget = "entity_reference_autocomplete",
 *   default_formatter = "entity_reference_label",
 *   list_class = "\Drupal\taxonomy_enum\Plugin\Field\FieldType\TaxonomyEnumItemList",
 * )
 */
class TaxonomyEnumItem extends EntityReferenceItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'target_type' => 'taxonomy_term',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    $properties['enum'] = DataDefinition::create('any')
      ->setLabel(t('Enum'))
      ->setDescription(t('An instance of the enum.'))
      ->setComputed(TRUE)
      ->setClass(ComputedTaxonomyEnum::class);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);

    // This should always be the default value.
    $element['target_type']['#disabled'] = TRUE;

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    if ($values instanceof \BackedEnum) {
      $values = \Drupal::getContainer()
        ->get('taxonomy_enum.manager')
        ->getTermByEnum($values)->id();
    }

    parent::setValue($values, $notify);
  }

  /**
   * {@inheritdoc}
   */
  public static function getPreconfiguredOptions() {
    return [];
  }

}
