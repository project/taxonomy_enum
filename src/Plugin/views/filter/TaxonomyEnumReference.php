<?php

namespace Drupal\taxonomy_enum\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\taxonomy\TermInterface;
use Drupal\views\Plugin\views\filter\ManyToOne;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Filters a view by entity references.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("taxonomy_enum_reference")
 */
class TaxonomyEnumReference extends ManyToOne {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityTypeManager = $container->get('entity_type.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $machineNames = $this->value;
    $termIds = $this->entityTypeManager
      ->getStorage('taxonomy_term')
      ->getQuery()
      ->condition('vid', $this->options['vid'])
      ->condition('machine_name', $this->value, 'IN')
      ->accessCheck(FALSE)
      ->execute();

    $this->value = array_values($termIds);
    parent::query();
    $this->value = $machineNames;
  }

  /**
   * {@inheritdoc}
   */
  public function getValueOptions() {
    if ($this->valueOptions !== NULL && $this->valueOptions !== []) {
      return $this->valueOptions;
    }

    if (empty($this->options['vid'])) {
      return $this->valueOptions = [];
    }

    $vid = $this->options['vid'];
    $vocabulary = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->load($vid);
    $enumName = $vocabulary->getThirdPartySetting('taxonomy_enum', 'enum_name');

    if (!enum_exists($enumName) || !method_exists($enumName, 'from')) {
      return $this->valueOptions = [];
    }

    $termStorage = $this->entityTypeManager
      ->getStorage('taxonomy_term');
    $termIds = $termStorage->getQuery()
      ->condition('vid', $vid)
      ->accessCheck(FALSE)
      ->execute();
    $terms = $termStorage->loadMultiple($termIds);

    return $this->valueOptions = array_reduce(
      $terms,
      function (?array $options, TermInterface $term) {
        $options[$term->get('machine_name')->value] = $term->label();
        return $options;
      },
    );
  }

  /**
   * {@inheritdoc}
   */
  public function hasExtraOptions() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildExtraOptionsForm(&$form, FormStateInterface $form_state) {
    $form['vid'] = [
      '#type' => 'select',
      '#title' => $this->t('Vocabulary'),
      '#options' => array_map(
        function ($vocabulary) {
          return $vocabulary->label();
        },
        Vocabulary::loadMultiple(),
      ),
      '#default_value' => $this->options['vid'] ?? NULL,
    ];
  }

}
