<?php

namespace Drupal\taxonomy_enum;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\taxonomy_enum\Exception\InvalidEnumException;

/**
 * The taxonomy enum manager service.
 */
class TaxonomyEnumManager implements TaxonomyEnumManagerInterface {

  /**
   * Constructs a new TaxonomyEnumSync object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function getTermByEnum(\BackedEnum $enum): ?TermInterface {
    $vocabularyId = $this->getVocabularyIdByEnumName(get_class($enum));
    if (!$vocabularyId === NULL) {
      return NULL;
    }

    $storage = $this->entityTypeManager
      ->getStorage('taxonomy_term');
    $query = $storage->getQuery()
      ->condition('vid', $vocabularyId)
      ->condition('machine_name', $enum->value)
      ->accessCheck(FALSE);
    $ids = $query->execute();

    if ($ids === []) {
      $this->createTerms($vocabularyId, get_class($enum));
      $ids = $query->execute();
    }

    if ($ids === []) {
      throw new \RuntimeException(
        sprintf('Failed to create term for enum %s::%s', get_class($enum), $enum->name)
      );
    }

    return $storage->load(reset($ids));
  }

  /**
   * {@inheritdoc}
   */
  public function getVocabularyIdByEnumName(string $enumName): ?string {
    $this->validateEnum($enumName);

    $storage = $this->entityTypeManager
      ->getStorage('taxonomy_vocabulary');
    $ids = $storage->getQuery()
      ->condition('third_party_settings.taxonomy_enum.enum_name', $enumName)
      ->accessCheck(FALSE)
      ->execute();

    if ($ids === []) {
      return NULL;
    }

    return reset($ids);
  }

  /**
   * {@inheritdoc}
   */
  public function createTerms(string $vocabularyId, string $enumName): void {
    $this->validateEnum($enumName);
    $machineNames = array_map(
      fn (\BackedEnum $item) => $item->value,
      $enumName::cases()
    );

    if ($machineNames === []) {
      return;
    }

    $storage = $this->entityTypeManager
      ->getStorage('taxonomy_term');

    $existingTermMachineNames = [];
    $existingTermIds = $storage->getQuery()
      ->condition('vid', $vocabularyId)
      ->condition('machine_name', $machineNames, 'IN')
      ->accessCheck(FALSE)
      ->execute();

    if ($existingTermIds !== []) {
      $existingTerms = $storage->loadMultiple($existingTermIds);
      $existingTermMachineNames = array_map(
        fn (TermInterface $term) => $term->get('machine_name')->value,
        $existingTerms
      );
    }

    $newTermMachineNames = array_diff($machineNames, $existingTermMachineNames);
    foreach ($newTermMachineNames as $newTermMachineName) {
      $storage->create([
        'vid' => $vocabularyId,
        'name' => $newTermMachineName,
        'machine_name' => $newTermMachineName,
      ])->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteOrphanedTerms(string $enumName): void {
    $this->validateEnum($enumName);
    $vocabularyId = $this->getVocabularyIdByEnumName($enumName);
    $machineNames = array_map(
      fn (\BackedEnum $item) => $item->value,
      $enumName::cases()
    );

    $storage = $this->entityTypeManager
      ->getStorage('taxonomy_term');
    $query = $storage->getQuery()
      ->condition('vid', $vocabularyId)
      ->accessCheck(FALSE);

    if ($machineNames !== []) {
      $query->condition('machine_name', $machineNames, 'NOT IN');
    }

    $ids = $query->execute();
    if ($ids === []) {
      return;
    }

    $entities = $storage->loadMultiple($ids);
    $storage->delete($entities);
  }

  /**
   * Validates the given enum name.
   */
  protected function validateEnum(string $enumName): void {
    if (!enum_exists($enumName) || !method_exists($enumName, 'cases')) {
      throw new InvalidEnumException(
        sprintf("'%s' is not a valid backed enum.", $enumName)
      );
    }

    foreach ($enumName::cases() as $case) {
      if (!preg_match('/^[a-z0-9_]+$/', $case->value)) {
        throw new InvalidEnumException(
          sprintf('Invalid machine name "%s". Machine names can only contain lowercase letters, numbers, and underscores.', $case->value)
        );
      }
    }
  }

}
